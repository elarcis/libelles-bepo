# Libellés BÉPO

![Preview de la disposition](Preview.png)

Ce dépôt héberge les fichiers SVG pour imprimer des libellés de touches de clavier basés sur [la disposition bépo](https://bepo.fr).

Les fichiers ont été à l’origine conçus pour le site [wasdkeyboards.com](https://www.wasdkeyboards.com), sur des touches de couleurs noires, blanches et oranges, exactement telles que décrites dans les SVG. Toute autre coloration de touches nécessitera une modification desdits SVG au risque que les libellés soient illisibles.

## Fichiers

Deux versions sont mises à disposition :

- [bépo_105_fontless.svg](https://gitlab.com/elarcis/libelles-bepo/-/blob/master/b%C3%A9po_105_fontless.svg), un fichier SVG dont tous les textes ont été vectorisés. Il ne nécessite aucune font installée.
- [bépo_105.svg](https://gitlab.com/elarcis/libelles-bepo/-/blob/master/b%C3%A9po_105.svg), un fichier SVG pouvant être facilement modifié. Il nécessite les polices [Iosevka Aile](https://typeof.net/Iosevka/) et [Font Awesome Pro](https://fontawesome.com/) (version 5 ou supérieure).

Chaque SVG dispose de plusieurs calques pour désactiver à volonté les guides, la grille ou l’arrière-plan.

Le dépôt inclut également le fichier de travail original [Affinity Designer 2](https://affinity.serif.com/fr/designer/).
